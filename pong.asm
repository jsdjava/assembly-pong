use16	;16 bit command for the assembler, not really sure what the differences are

org 0x7C00	;Starting address, bios checks here for an img to load, could also just jump to it 

speed = 5	;All pong paddles move at this speed
board_width = 10
board_height = 30
max_y = 200-board_height
ball_radius = 2

mov ax,0013h	;Set to vga mode 320x200
int 10h	;Enter 320x200 graphics mode for vga
;ax holds the rect on the lefts position, bx holds the rect on the rights position
mov ax,85
mov bx,85
mov cx,160
mov dx,100
push 0 ;xCounter
push 0; yCounter
push 3; xSpeed
push 2; ySpeed
push 0; xDir (or FFh)
push 0; yDir (or FFh)

mainLoop:
  ;move ball (in the x)
  push bp
  mov bp,sp
  push ax
  push bx
  mov ax,[bp+12]
  mov bx,[bp+4]
  inc ax
  cmp ax, [bp+8]
  jle end_ball_x_move
  test bx,bx
  jz inc_ball_x
  dec cx
  jmp reset_ball_x_counter
  inc_ball_x:
    inc cx
  reset_ball_x_counter:
    mov ax,0
  end_ball_x_move:
  mov [bp+12],ax
  ; move ball (in the y)
  mov ax,[bp+10]
  mov bx,[bp+2]
  inc ax
  cmp ax,[bp+6]
  jle end_ball_y_move
  test bx,bx
  jz inc_ball_y
  dec dx
  jmp reset_ball_y_counter
  inc_ball_y:
    inc dx
  reset_ball_y_counter:
    mov ax,0
  end_ball_y_move:
  mov [bp+10],ax
  ; bounce in the y
  test dx,dx
  jz bounce_y
  cmp dx,198 ; screen height - ball radius
  jge bounce_y
  jmp end_bounce_y
  bounce_y:
    not bx
    mov [bp+2],bx
  end_bounce_y:
  ;bounce in the x
  mov ax,[bp-2]
  cmp cx,board_width
  jle check_bounce_x
  mov ax,[bp-4]
  cmp cx,320-ball_radius-board_width-1
  jl end_bounce_x
  check_bounce_x:
  add ax,board_height
  cmp dx,ax
  jg score
  sub ax,board_height
  add dx,ball_radius
  cmp dx,ax
  pushf
  sub dx,ball_radius
  popf
  jl score
  jmp bounce_x
  score:
    mov cx,160
    mov dx,100
  jmp end_bounce_x
  bounce_x:
    mov ax, [bp+4]
    not ax
    mov [bp+4],ax
    mov ax, [bp+2]
    not ax
    mov [bp+2],ax
    mov ax, [bp+8]
    mov [bp+12],ax
    mov ax, [bp+6]
    mov [bp+10],ax
  end_bounce_x:
  pop bx
  pop ax
  pop bp
  ; sleep for 10 ms
  push ax
  push cx
  push dx
  mov al, 0
  mov ah, 86h
  mov cx, 0
  mov dx, 2710h
  int 15h
  pop dx
  pop cx
  pop ax
  ;Handle keypresses
  push ax
  mov al,0
  mov ah,1
  int 16h
  jz no_key_pressed
  mov ah,0
  int 16h
  cmp al,115; ASCII for s
  je s_pressed
  cmp al,119; ASCII for w
  je w_pressed
  cmp al,107; ASCII for k
  je k_pressed
  cmp al,105; ASCII for i
  je i_pressed
  ;no interesting key was pressed
  pop ax
  jmp end_key_checks

  no_key_pressed:
    pop ax
    jmp end_key_checks

  k_pressed:
    pop ax
    cmp bx, max_y-speed
    jl after_bounds_check_k
    mov bx,max_y
    jmp end_key_checks
    after_bounds_check_k:
      add bx,speed
      jmp end_key_checks

  i_pressed:
    pop ax
    cmp bx, speed
    jg after_bounds_check_i
    mov bx,0
    jmp end_key_checks
    after_bounds_check_i:
      sub bx,speed
      jmp end_key_checks

  s_pressed:
    pop ax
    cmp ax, max_y-speed
    jl after_bounds_check_s
    mov ax,max_y
    jmp end_key_checks
    after_bounds_check_s:
      add ax,speed
      jmp end_key_checks

  w_pressed:
    pop ax
    cmp ax, speed
    jg after_bounds_check_w
    mov ax,0
    jmp end_key_checks
    after_bounds_check_w:
      sub ax,speed

  end_key_checks:
    push bp
    mov bp,sp
    ;Draw the ball
    push 9
    push cx
    push dx
    push 2
    push 2
    push 1
    call drawRect
    ;Draw the 1st rectangle
    mov sp,bp
    push 2
    push 0
    push ax
    push board_width
    push board_height
    push speed
    call drawRect
    mov sp,bp;Reuse the old stack instead of just pushing more on
    ;Draw the 2nd rectangle
    push 5
    push 309
    push bx
    push board_width
    push board_height
    push speed
    call drawRect
    mov sp,bp
    pop bp

jmp mainLoop	;Jumps to start of an instruction, in this case obviously the same spot, inf loop

;bp,color,x,y,width,height, + speed
drawRect:	
  push ax
  push bx
  push cx
  push dx
  ; calculate the max(0,x-speed)
  mov cx,[bp-4]
  cmp cx,[bp-12]
  jg sub_min_x
  zero_min_x:
    mov cx,0
    jmp end_min_x
  sub_min_x:
    sub cx,[bp-12]
  end_min_x:
  ; calculate the max(0,y-speed)
  mov dx,[bp-6]
  cmp dx,[bp-12]
  jg sub_min_y
  zero_min_y:
    mov dx,0
    jmp end_min_y
  sub_min_y:
    sub dx,[bp-12]
  end_min_y:
  push cx
  push dx
  ; calculate the min(x+width+speed,screen_width)
  mov cx,[bp-4]
  add cx,[bp-8]
  add cx,[bp-12]
  cmp cx,320
  jle end_max_x
  mov cx,320
  end_max_x:
  ; calculate the min(y+height+speed,screen_height)
  mov dx,[bp-6]
  add dx,[bp-10]
  add dx,[bp-12]
  cmp dx,200
  jle end_max_y
  mov dx,200
  end_max_y:
  push cx
  push dx
  ; calculate x+width and y+height
  mov cx,[bp-4]
  mov dx,[bp-6]
  add cx,[bp-8]
  add dx,[bp-10]
  push cx
  push dx
  ; setup for rectangle draw loop
  mov ah,0ch
  xor bx,bx
  mov cx,[bp-24]
  mov dx,[bp-26]

  ; iterate from minX to maxX, minY to maxY
  ; if cx>=x && cx<x+width && dx>=y && dx < y+height
  ; set color to color 
  ; else set color to black

  yLoop:
  xLoop:
  ; determine color
  cmp cx,[bp-4]
  jl set_black
  cmp dx,[bp-6]
  jl set_black
  cmp cx,[bp-32]
  jge set_black
  cmp dx,[bp-34]
  jge set_black
  mov al, [bp-2]
  jmp draw_pixel
  set_black:
  mov al,0
  draw_pixel:
  int 10h
  inc cx
  cmp cx,[bp-28]
  jl xLoop
  mov cx,[bp-24]
  inc dx
  cmp dx,[bp-30]
  jl yLoop  

  ;end loop
  pop dx
  pop dx
  pop dx
  pop dx
  pop dx
  pop dx
  pop dx
  pop cx
  pop bx
  pop ax
ret 

TIMES 510 - ($-$$) DB  0	;So $ start of this current instruction, $$ is the start of the program, we're filling the rest of it with zeros

DW 0xAA55	;Last two bytes get a signature that identify this as a bootloader