@echo off

echo Setting Color

echo Setting File Name Variables
set "outFN=%1.img"
set "inFN=%1.asm"

echo %inFN%

echo Compiling In FASM
"fasm" %inFN% %outFN%

echo Launching In QEMU
qemu-system-x86_64.exe -boot a -fda %outFN%