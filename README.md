# Assembly Pong
## Demo

[![Thumbnail](pics/thumbnail.png)](https://drive.google.com/file/d/1SBsXkDYGMMAr-V-Vb75JMMouDOdznGNT/view?usp=sharing "Assembly Pong - Click to open video")
[Click image (or here) to open video](https://drive.google.com/file/d/1SBsXkDYGMMAr-V-Vb75JMMouDOdznGNT/view?usp=sharing)

## Description
Assembly Pong is a project I wrote in assembly because I wanted to make my own very simple operating system.
To avoid having to load other sectors after the first sector is bootloaded, I kept it under 512 bytes. 
The image can actually be burned to a flash drive and booted up on a computer, as shown in the demo.

Major features completed are:
* Paddle rendering
* Ball rendering
* Ball movement
* Ball bouncing
* Paddle movement
* Scoring

Other features I may implement in the future are:
* Keeping track of the score
* Game win/loss
* Increasing/randomizing ball speed and bounce

One problem is that I ran out of space with my current implementation, so I will
need to either make the code smaller somehow or load another sector up.

## Running Locally
I used fasm to compile assembly-pong, and qemu to emulate running it in development
The img file produced by fasm can also be burned to a flash drive, which can be booted.

On windows, I included a batch file helper to compile and run:

`launch pong`
